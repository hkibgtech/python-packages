#hkibg qlcoud cos keeper

The purpose of this package is to provide scripts for handling log and
data files, it can upload the scripts to tencent cloud cos automatically

three scripts are included in this package:
- qcloud_cos_keeper_file_cleaner.py (clean up files older than a user defined time)
- qcloud_cos_keeper_file_rotator.py (move file with a date extension)
- qcloud_cos_keeper_file_uploader.py (upload the file to qcloud cos)
