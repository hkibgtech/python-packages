#/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
python3 $DIR/setup.py sdist bdist_wheel
pip install $DIR/dist/joox_web_crawler-*.tar.gz
